# EasyPermissions 
#### 项目介绍

- 项目名称：EasyPermissions
- 所属系列：openharmony的第三方组件适配移植
- 功能：动态权限申请
- 项目移植状态：主功能完成
- 调用差异：有（因为受限权限原因）
- 开发版本：sdk6，DevEco Studio2.2 beta2
- 基线版本：Release 3.0.0

#### 演示效果
![](img/sample.gif)

#### 安装教程

1.在项目根目录下的 `build.gradle` 文件中，

```
// 添加maven仓库
repositories {
    maven {
        url 'https://s01.oss.sonatype.org/content/repositories/release/'
    }
}
```
2.在entry模块的 `build.gradle` 文件中，
```
// 添加依赖库
dependencies {
    implementation 'com.gitee.chinasoft_ohos:easypermissions:1.0.0'
}
 ```

在sdk6，DevEco Studio2.2 beta1下项目可直接运行 如无法运行，删除项目.gradle,.idea,build,gradle,
build.gradle文件， 并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下

#### 使用说明

- 使用EasyPermissions, 需要你在MainAbility 重写onRequestPermissionsFromUserResult 方法,并在
MainAbilitySlice的onRequestPermissionsResult调用EasyPermissions.onRequestPermissionsResult()
方法。

```
public class MainAbility extends Ability {
    MainAbilitySlice abilitySlice;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(MainAbilitySlice.class.getName());
    }
    
    ...
    @Override
    public void onRequestPermissionsFromUserResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsFromUserResult(requestCode, permissions, grantResults);
        //传递到AbilitySlice里面，onRequestPermissionsResult（可以自定义）
        abilitySlice.onRequestPermissionsResult(requestCode, permissions, grantResults);
	}
	...
}

	//在AbilitySlice里面添加
	public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }
```

- 权限请求

  下面的示例显示了如何为需要这两者的方法请求权限“相机”和“定位”的权限。有几件事需要注意:

  * 使用EasyPermissions.hasPermissions()检查应用程序是否已经有所需的权限。这个方法可以接受任意数量的权限作为参数。

  * 使用EasyPermissions.requestPermissions()请求权限，调出出用户授权界面。

  * 用户授权回调

   ```java
    public class MainAbilitySlice extends AbilitySlice {
  
        @Override
        public void onStart(Intent intent) {
            super.onStart(intent);
            ...
            //请求权限
            if (EasyPermissions.hasPermissions(this, "请求权限")) {
              // 已经有授权
            } else {
              // 请求授权
              EasyPermissions.requestPermissions(this.getAbility(), 
                  "请求权限弹框的内容","事件编号（int值）", "请求权限");
            }
        }
        ...
        
        @Override
        public void onPermissionsGranted(int requestCode, List<String> list) {
            // 用户允许授权回调
        }
        
        @Override
        public void onPermissionsDenied(int requestCode, List<String> list) {
            // 用户拒绝授权回调（弹出引导弹窗）
        }
  
    }
   ```

- 在用户对权限选择禁止且不在提示时，弹出自定义的弹窗引导用户在设置界面打开权限：

  ```
  /**
  *perms String[] 请求的权限
  */
  if (EasyPermissions.somePermissionPermanentlyDenied(mAbility, perms)) {
      PermissionDialog dialog = new PermissionDialog(mContext, requestCode, str);
      dialog.setTitle("弹窗标题");
      dialog.setContent("弹窗内容");
      //弹窗的确定和取消的点击shi'jian2
      dialog.setOnClickListener(new PermissionDialog.OnClickListener() {
          @Override
          public void ok(IDialog myDialog) {
              //点击确定 跳转到设置页面
              Intent intent = new Intent();
              intent.setAction(IntentConstants.ACTION_APPLICATION_DETAILS_SETTINGS);
              intent.setUri(Uri.parse("package:" + mContext.getBundleName()));
              mContext.startAbility(intent, REQUEST_SETTING);
          }
  
          @Override
          public void cancel(IDialog myDialog) {
          	//点击取消
          }
      });
      dialog.show();
  }
  ```



#### 测试信息

CodeCheck代码测试无异常 
 
CloudTest代码测试无异常  

病毒安全检测通过  

当前版本demo功能与原组件基本无差异  

#### 版本迭代

- 1.0.0  

#### 版权和许可信息  

```
	Copyright 2017 Google

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
```
