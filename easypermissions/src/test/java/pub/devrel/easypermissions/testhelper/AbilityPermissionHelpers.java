package pub.devrel.easypermissions.testhelper;

import ohos.aafwk.ability.Ability;
import ohos.agp.components.Component;
import ohos.agp.window.dialog.CommonDialog;
import ohos.app.AbilityContext;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

import pub.devrel.easypermissions.helper.PermissionHelper;

/**
 * Permissions helper for {@link Ability}.
 */
public class AbilityPermissionHelpers extends PermissionHelper<Ability> {
    private static final HiLogLabel TAG = new HiLogLabel(HiLog.LOG_APP, 0x00201, "MY_TAG");
    private static final int DIALOG_SIZE = 100;

    public AbilityPermissionHelpers(Ability host) {
        super(host);
    }

    @Override
    public void directRequestPermissions(int requestCode, String... perms) {
        getHost().requestPermissionsFromUser(perms, requestCode);
    }

    @Override
    public boolean shouldShowRequestPermissionRationale(String perm) {
        return getHost().canRequestPermission(perm);
    }

    @Override
    public AbilityContext getContext() {
        return (AbilityContext) getHost().getContext();
    }

    @Override
    public void showRequestPermissionRationale(String rationale,
                                               String positiveButton,
                                               String negativeButton,
                                               int theme,
                                               int requestCode,
                                               String... perms) {

        CommonDialog commonDialog = new CommonDialog(getContext());
        Component component = new Component(getContext());
        commonDialog.setContentText(rationale);
        commonDialog.setSize(DIALOG_SIZE, DIALOG_SIZE);
        commonDialog.setContentCustomComponent(component);
        commonDialog.show();
    }

    public static class newInstance {

        public newInstance(String title) {

        }
    }
}
