package pub.devrel.easypermissions;


import ohos.app.AbilityContext;

import org.junit.Before;
import org.junit.Test;

import pub.devrel.easypermissions.testhelper.EasyPermissionss;

import static com.google.common.truth.Truth.assertThat;

//不支持Mockito
public class EasyPermissionsTest {
    private AbilityContext app;
    private static final String[] ALL_PERMS = new String[]{
        "ohos.permission.READ_SMS", "ohos.permission.ACCOUNT_MANAGER"};

    @Before
    public void setUp() {

    }

    @Test
    public void shouldThrowException_whenHasPermissionsWithNullContext() {
        try {
            EasyPermissionss.hasPermissions(null, ALL_PERMS);
          //  fail("IllegalStateException expected because of null context.");
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessageThat()
                .isEqualTo("Can't check permissions for null context");
        }
    }
}
