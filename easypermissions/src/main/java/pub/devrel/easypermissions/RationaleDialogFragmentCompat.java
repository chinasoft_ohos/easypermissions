package pub.devrel.easypermissions;

import ohos.aafwk.ability.fraction.Fraction;
import ohos.agp.window.dialog.CommonDialog;
import ohos.agp.window.dialog.IDialog;

/**
 * {@link Fraction} to display rationale for permission requests when the request
 * comes from a Fragment or Ability that can host a Fragment.
 */
public class RationaleDialogFragmentCompat extends Fraction
    implements CommonDialog.DestroyedListener,CommonDialog.RemoveCallback {
    public static final String TAG = "RationaleDialogFragmentCompat";

    private EasyPermissions.PermissionCallbacks mPermissionCallbacks;
    private EasyPermissions.RationaleCallbacks mRationaleCallbacks;

    public static RationaleDialogFragmentCompat newInstance(
            String rationaleMsg,
            String positiveButton,
            String negativeButton,
            int theme,
            int requestCode,
            String[] permissions) {
        // Create new Fragment
        RationaleDialogFragmentCompat dialogFragment = new RationaleDialogFragmentCompat();

        // Initialize configuration as arguments
        RationaleDialogConfig config = new RationaleDialogConfig(
                positiveButton, negativeButton, rationaleMsg, theme, requestCode, permissions);
        return dialogFragment;
    }

    /**
     * Version of {@link #} that no-ops when an IllegalStateException
     * would otherwise occur.
     */
    public void showAllowingStateLoss() {
    }

    @Override
    protected void onComponentDetach() {
        super.onComponentDetach();
        mPermissionCallbacks = null;
        mRationaleCallbacks = null;
    }

    @Override
    public void onRemove(IDialog myDialog) {
    }

    @Override
    public void onDestroy() {
    }
}
