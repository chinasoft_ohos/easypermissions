package pub.devrel.easypermissions;

import ohos.aafwk.ability.fraction.Fraction;
import ohos.aafwk.ability.fraction.FractionManager;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;

/**
 * {@link Fraction} to display rationale for permission requests when the request comes from
 * a Fragment or Ability that can host a Fragment.
 */
public class RationaleDialogFragment extends Fraction {

    public static final String TAG = "RationaleDialogFragment";

    private EasyPermissions.PermissionCallbacks mPermissionCallbacks;
    private EasyPermissions.RationaleCallbacks mRationaleCallbacks;
    private boolean mStateSaved = false;

    public static RationaleDialogFragment newInstance(
            String positiveButton,
            String negativeButton,
            String rationaleMsg,
            int theme,
            int requestCode,
            String[] permissions) {
        // Create new Fragment
        RationaleDialogFragment dialogFragment = new RationaleDialogFragment();

        // Initialize configuration as arguments
        RationaleDialogConfig config = new RationaleDialogConfig(
                positiveButton, negativeButton, rationaleMsg, theme, requestCode, permissions);
        return dialogFragment;
    }

    @Override
    protected Component onComponentAttached(LayoutScatter scatter, ComponentContainer container, Intent intent) {
        return super.onComponentAttached(scatter, container, intent);
    }

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
    }

    /**
     * Version of {@link } that no-ops when an IllegalStateException
     * would otherwise occur.
     *
     * @param manager Fraction管理器
     * @param tag 标识
     */
    public void showAllowingStateLoss(FractionManager manager, String tag) {
        if (mStateSaved) {
            return;
        }
    }

    @Override
    protected void onComponentDetach() {
        super.onComponentDetach();
        mPermissionCallbacks = null;
    }
}
