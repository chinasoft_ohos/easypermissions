/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package pub.devrel.easypermissions;

import ohos.agp.components.AttrHelper;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.Text;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.utils.Point;
import ohos.agp.window.dialog.CommonDialog;
import ohos.agp.window.dialog.IDialog;
import ohos.agp.window.service.DisplayManager;
import ohos.app.Context;

/**
 * 自定义弹窗
 *
 * @since 2021-03-05
 */
public class PermissionDialog extends CommonDialog {
    private static final int REQUEST_SETTING = 300;
    private static final int LAYOUT_SPACE = 60;
    private Text text;
    private Text title;
    private Context mContext;
    private int requestCode;
    private String[] permissions;
    private OnClickListener onClickListener;

    /**
     * 构造函数
     *
     * @param context 上下文
     * @param code 请求码
     * @param perms 请求权限
     */
    public PermissionDialog(Context context, int code, String... perms) {
        super(context);
        siteRemovable(false);
        mContext = context;
        this.requestCode = code;
        permissions = perms;
        setTransparent(true);
        setAlignment(LayoutAlignment.CENTER);
        Component component = LayoutScatter.getInstance(context).parse(ResourceTable.Layout_permission_dialog,
            null, true);
        setContentCustomComponent(component);
        initEvent(component);
        setSize(getWidth(context) - AttrHelper.vp2px(LAYOUT_SPACE, AttrHelper.getDensity(context)),
            ComponentContainer.LayoutConfig.MATCH_CONTENT);
        this.siteKeyboardCallback((iDialog, keyEvent) -> true);
    }

    public void setOnClickListener(OnClickListener onClickListener) {
        this.onClickListener = onClickListener;
    }

    /**
     * 获取屏幕宽度
     *
     * @param context 上下文
     * @return 屏幕宽度
     */
    public int getWidth(Context context) {
        Point point = new Point();
        DisplayManager.getInstance().getDefaultDisplay(context).get().getSize(point);
        return (int) point.getPointX();
    }

    private void initEvent(Component component) {
        component.findComponentById(ResourceTable.Id_ok).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                if (onClickListener != null) {
                    onClickListener.ok(PermissionDialog.this);
                }
                destroy();
            }
        });

        component.findComponentById(ResourceTable.Id_canle).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                if (onClickListener != null) {
                    onClickListener.cancel(PermissionDialog.this);
                }
                destroy();
            }
        });

        text = (Text) component.findComponentById(ResourceTable.Id_content);

        title = (Text) component.findComponentById(ResourceTable.Id_title);
    }

    /**
     * 设置弹窗内容
     *
     * @param string 设置的内容
     */
    public void setContent(String string) {
        this.text.setText(string);
    }

    /**
     * 设置弹窗标题
     *
     * @param string 设置的标题
     */
    public void setTitle(String string) {
        this.title.setText(string);
        title.setVisibility(Component.VISIBLE);
    }

    /**
     * 自定义弹窗的选择监听
     *
     * @since 2021-03-05
     */
    public interface OnClickListener {
        /**
         * 确定按钮
         *
         * @param myDialog
         */
        void ok(IDialog myDialog);

        /**
         * 取消按钮
         *
         * @param myDialog
         */
        void cancel(IDialog myDialog);
    }
}
