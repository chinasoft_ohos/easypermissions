package pub.devrel.easypermissions;

import ohos.agp.window.dialog.CommonDialog;
import ohos.agp.window.dialog.IDialog;
import ohos.app.AbilityContext;
import ohos.app.Context;

/**
 * Configuration for either {@link RationaleDialogFragment} or {@link RationaleDialogFragmentCompat}.
 */
class RationaleDialogConfig {

    private static final String KEY_POSITIVE_BUTTON = "positiveButton";
    private static final String KEY_NEGATIVE_BUTTON = "negativeButton";
    private static final String KEY_RATIONALE_MESSAGE = "rationaleMsg";
    private static final String KEY_THEME = "theme";
    private static final String KEY_REQUEST_CODE = "requestCode";
    private static final String KEY_PERMISSIONS = "permissions";
    private static final int DIALOG_SIZE = 100;

    String positiveButton;
    String negativeButton;
    int theme;
    int requestCode;
    String rationaleMsg;
    String[] permissions;

    RationaleDialogConfig(String positiveButton,
                          String negativeButton,
                          String rationaleMsg,
                          int theme,
                          int requestCode,
                          String[] permissions) {

        this.positiveButton = positiveButton;
        this.negativeButton = negativeButton;
        this.rationaleMsg = rationaleMsg;
        this.theme = theme;
        this.requestCode = requestCode;
        this.permissions = permissions;
    }

    CommonDialog createSupportDialog(AbilityContext context, IDialog.ClickedListener listener) {
        CommonDialog commonDialog = new CommonDialog(context);
        commonDialog.setButton(IDialog.BUTTON1, "OK", listener);
        commonDialog.setContentText(rationaleMsg);
        commonDialog.setSize(DIALOG_SIZE, DIALOG_SIZE);

        return commonDialog;
    }

    CommonDialog createFrameworkDialog(Context context, IDialog.ClickedListener listener) {
        CommonDialog commonDialog = new CommonDialog(context);

        commonDialog.setButton(IDialog.BUTTON1, "OK", listener);
        commonDialog.setContentText(rationaleMsg);
        commonDialog.setSize(DIALOG_SIZE, DIALOG_SIZE);
        return commonDialog;
    }
}
