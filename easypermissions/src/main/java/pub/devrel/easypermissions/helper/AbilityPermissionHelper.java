package pub.devrel.easypermissions.helper;

import ohos.aafwk.ability.Ability;
import ohos.agp.components.Component;
import ohos.agp.window.dialog.CommonDialog;
import ohos.app.AbilityContext;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

/**
 * Permissions helper for {@link Ability}.
 */
class AbilityPermissionHelper extends PermissionHelper<Ability> {
    private static final HiLogLabel TAG = new HiLogLabel(HiLog.LOG_APP, 0x00201, "MY_TAG");
    private static final int DIALOG_SIZE = 100;

    public AbilityPermissionHelper(Ability host) {
        super(host);
    }

    @Override
    public void directRequestPermissions(int requestCode, String... perms) {
        HiLog.info(TAG, "直接去申请权限 directRequestPermissions");
        getHost().requestPermissionsFromUser(perms, requestCode);
    }

    @Override
    public boolean shouldShowRequestPermissionRationale(String perm) {
        HiLog.info(TAG, "直接去申请权限 canRequestPermission = " + getHost().canRequestPermission(perm));
        return getHost().canRequestPermission(perm);
    }

    @Override
    public AbilityContext getContext() {
        return (AbilityContext) getHost().getContext();
    }

    @Override
    public void showRequestPermissionRationale(String rationale,
                                               String positiveButton,
                                               String negativeButton,
                                               int theme,
                                               int requestCode,
                                               String... perms) {
        HiLog.info(TAG, "弹出dialog AbilityPermissionHelper showRequestPermissionRationale");

        CommonDialog commonDialog = new CommonDialog(getContext());
        Component component = new Component(getContext());
        commonDialog.setContentText(rationale);
        commonDialog.setSize(DIALOG_SIZE, DIALOG_SIZE);
        commonDialog.setContentCustomComponent(component);
        commonDialog.show();
    }
}
