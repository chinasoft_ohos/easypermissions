package pub.devrel.easypermissions.helper;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.ability.fraction.Fraction;
import ohos.app.AbilityContext;

/**
 * Permissions helper for apps built against API < 23, which do not need runtime permissions.
 */
class LowApiPermissionsHelper<T> extends PermissionHelper<T> {
    public LowApiPermissionsHelper(T host) {
        super(host);
    }

    @Override
    public void directRequestPermissions(int requestCode, String... perms) {
        throw new IllegalStateException("Should never be requesting permissions on API < 23!");
    }

    @Override
    public boolean shouldShowRequestPermissionRationale(String perm) {
        return false;
    }

    @Override
    public void showRequestPermissionRationale(String rationale,
                                               String positiveButton,
                                               String negativeButton,
                                               int theme,
                                               int requestCode,
                                               String... perms) {
        throw new IllegalStateException("Should never be requesting permissions on API < 23!");
    }

    @Override
    public AbilityContext getContext() {
        if (getHost() instanceof Ability) {
            return (AbilityContext) getHost();
        } else if (getHost() instanceof Fraction) {
            return (AbilityContext) ((Fraction) getHost()).getFractionAbility().getContext();
        } else {
            throw new IllegalStateException("Unknown host: " + getHost());
        }
    }
}
