package pub.devrel.easypermissions.helper;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.ability.fraction.FractionManager;
import ohos.app.AbilityContext;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

/**
 * Permissions helper for {@link Ability}.
 */
class AppCompatAbilityPermissionsHelper extends BaseSupportPermissionsHelper<Ability> {
    private static final HiLogLabel TAG = new HiLogLabel(HiLog.LOG_APP, 0x00201, "MY_TAG");

    public AppCompatAbilityPermissionsHelper(Ability host) {
        super(host);
    }

    @Override
    public FractionManager getSupportFragmentManager() {
        return (FractionManager) getHost().getAbilityManager();
    }

    @Override
    public void directRequestPermissions(int requestCode, String... perms) {
        HiLog.info(TAG, "弹出dialog AppCompatAbilityPermissionsHelper directRequestPermissions");
        getContext().requestPermissionsFromUser(perms, requestCode);
    }

    @Override
    public boolean shouldShowRequestPermissionRationale(String perm) {
        return getHost().canRequestPermission(perm);
    }

    @Override
    public AbilityContext getContext() {
        return (AbilityContext) getHost();
    }
}
