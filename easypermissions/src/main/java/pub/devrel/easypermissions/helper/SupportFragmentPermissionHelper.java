package pub.devrel.easypermissions.helper;

import ohos.aafwk.ability.fraction.Fraction;
import ohos.aafwk.ability.fraction.FractionManager;
import ohos.app.AbilityContext;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

/**
 * Permissions helper for {@link Fraction} from the support library.
 */
class SupportFragmentPermissionHelper extends BaseSupportPermissionsHelper<Fraction> {
    private static final HiLogLabel TAG = new HiLogLabel(HiLog.LOG_APP, 0x00201, "MY_TAG");

    public SupportFragmentPermissionHelper(Fraction host) {
        super(host);
    }

    @Override
    public FractionManager getSupportFragmentManager() {
        return getHost().getFractionAbility().getFractionManager();
    }

    @Override
    public void directRequestPermissions(int requestCode, String... perms) {
        HiLog.info(TAG, "直接去申请权限 SupportFragmentPermissionHelper directRequestPermissions");
        getHost().getFractionAbility().requestPermissionsFromUser(perms, requestCode);
    }

    @Override
    public boolean shouldShowRequestPermissionRationale(String perm) {
        HiLog.info(TAG, "申请权限shouldShowRequestPermissionRationale=" + getHost().canRequestPermission(perm));
        return getHost().canRequestPermission(perm);
    }

    @Override
    public AbilityContext getContext() {
        return (AbilityContext) getHost().getFractionAbility().getContext();
    }
}
