package pub.devrel.easypermissions.helper;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.ability.fraction.FractionManager;
import ohos.agp.window.dialog.IDialog;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

import pub.devrel.easypermissions.PermissionDialog;

/**
 * Implementation of {@link PermissionHelper} for Support Library host classes.
 */
public abstract class BaseSupportPermissionsHelper<T> extends PermissionHelper<T> {

    private static final HiLogLabel TAG = new HiLogLabel(HiLog.LOG_APP, 0x00201, "MY_TAG");

    public BaseSupportPermissionsHelper(T host) {
        super(host);
    }

    public abstract FractionManager getSupportFragmentManager();

    @Override
    public void showRequestPermissionRationale(String rationale,
                                               String positiveButton,
                                               String negativeButton,
                                               int theme,
                                               int requestCode,
                                               String... perms) {
        HiLog.info(TAG, "弹出dialog BaseSupportPermissionsHelper showRequestPermissionRationale");
        PermissionDialog dialog = new PermissionDialog(getContext(), requestCode, perms);
        dialog.setContent(rationale);
        dialog.setOnClickListener(new PermissionDialog.OnClickListener() {
            @Override
            public void ok(IDialog myDialog) {
                PermissionHelper.newInstance((Ability) getContext()).directRequestPermissions(requestCode, perms);
            }

            @Override
            public void cancel(IDialog myDialog) {
            }
        });
        dialog.show();
    }
}
