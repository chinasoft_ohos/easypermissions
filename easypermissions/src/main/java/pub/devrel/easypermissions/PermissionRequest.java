package pub.devrel.easypermissions;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.ability.fraction.Fraction;
import pub.devrel.easypermissions.helper.PermissionHelper;

import java.util.Arrays;

/**
 * An immutable model object that holds all of the parameters associated with a permission request,
 * such as the permissions, request code, and rationale.
 *
 * @see EasyPermissions#requestPermissions(PermissionRequest)
 * @see Builder
 */
public final class PermissionRequest {
    private final PermissionHelper mHelper;
    private final String[] mPerms;
    private final int mRequestCode;
    private final String mRationale;
    private final String mPositiveButtonText;
    private final String mNegativeButtonText;
    private final int mTheme;

    private PermissionRequest(PermissionHelper helper,
                              String[] perms,
                              int requestCode,
                              String rationale,
                              String positiveButtonText,
                              String negativeButtonText,
                              int theme) {
        mHelper = helper;
        mPerms = perms.clone();
        mRequestCode = requestCode;
        mRationale = rationale;
        mPositiveButtonText = positiveButtonText;
        mNegativeButtonText = negativeButtonText;
        mTheme = theme;
    }

    // @RestrictTo(RestrictTo.Scope.LIBRARY_GROUP)
    public PermissionHelper getHelper() {
        return mHelper;
    }

    public String[] getPerms() {
        return mPerms.clone();
    }

    public int getRequestCode() {
        return mRequestCode;
    }

    public String getRationale() {
        return mRationale;
    }

    public String getPositiveButtonText() {
        return mPositiveButtonText;
    }

    public String getNegativeButtonText() {
        return mNegativeButtonText;
    }

    public int getTheme() {
        return mTheme;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PermissionRequest request = (PermissionRequest) o;

        return Arrays.equals(mPerms, request.mPerms) && mRequestCode == request.mRequestCode;
    }

    @Override
    public int hashCode() {
        int result = Arrays.hashCode(mPerms);
        result = 31 * result + mRequestCode;
        return result;
    }

    @Override
    public String toString() {
        return "PermissionRequest{" +
                "mHelper=" + mHelper +
                ", mPerms=" + Arrays.toString(mPerms) +
                ", mRequestCode=" + mRequestCode +
                ", mRationale='" + mRationale + '\'' +
                ", mPositiveButtonText='" + mPositiveButtonText + '\'' +
                ", mNegativeButtonText='" + mNegativeButtonText + '\'' +
                ", mTheme=" + mTheme +
                '}';
    }

    /**
     * Builder to build a permission request with variable options.
     *
     * @see PermissionRequest
     */
    public static final class Builder {
        private final PermissionHelper mHelper;
        private final int mRequestCode;
        private final String[] mPerms;

        private String mRationale;
        private String mPositiveButtonText;
        private String mNegativeButtonText;
        private int mTheme = -1;

        /**
         * Construct a new permission request builder with a host, request code, and the requested
         * permissions.
         *
         * @param ability    the permission request host
         * @param requestCode request code to track this request; must be &lt; 256
         * @param perms       the set of permissions to be requested
         */
        public Builder(Ability ability, int requestCode, String... perms) {
            mHelper = PermissionHelper.newInstance(ability);
            mRequestCode = requestCode;
            mPerms = perms;
        }

        /**
         * @see #Builder(Fraction, int, String...)
         */
        public Builder(Fraction fragment, int requestCode, String... perms) {
            mHelper = PermissionHelper.newInstance(fragment);
            mRequestCode = requestCode;
            mPerms = perms;
        }

        /**
         * Set the rationale to display to the user if they don't allow your permissions on the
         * first try. This rationale will be shown as long as the user has denied your permissions
         * at least once, but has not yet permanently denied your permissions. Should the user
         * permanently deny your permissions, use the {@link AppSettingsDialog} instead.
         * <p>
         * The default rationale text is {@link R.string#rationale_ask}.
         *
         * @param rationale the rationale to be displayed to the user should they deny your
         *                  permission at least once
         * @return builder
         */

        public Builder setRationale(String rationale) {
            mRationale = rationale;
            return this;
        }

        /**
         * Set the rationale
         *
         * @param resId the string resource to be used as a rationale
         * @see #setRationale(String)
         * @return builder
         */

        public Builder setRationale(int resId) {
            mRationale = mHelper.getContext().getString(resId);
            return this;
        }

        /**
         * Set the positive button text for the rationale dialog should it be shown.
         * <p>
         * The default is {@link "ok"}
         *
         * @param positiveButtonText 按钮上的文职
         * @return builder
         */

        public Builder setPositiveButtonText(String positiveButtonText) {
            mPositiveButtonText = positiveButtonText;
            return this;
        }

        /**
         * Set the positive button text
         * @see #setPositiveButtonText(String)
         *
         * @param resId 控件id
         * @return builder
         */

        public Builder setPositiveButtonText(int resId) {
            mPositiveButtonText = mHelper.getContext().getString(resId);
            return this;
        }

        /**
         * Set the negative button text for the rationale dialog should it be shown.
         * <p>
         * The default is {@link "cancel"}
         *
         * @param negativeButtonText 按钮文字说明
         * @return builder
         */

        public Builder setNegativeButtonText(String negativeButtonText) {
            mNegativeButtonText = negativeButtonText;
            return this;
        }

        /**
         * Set the negative button text
         * @see #setNegativeButtonText(String)
         *
         * @param resId 控件id
         * @return builder
         */

        public Builder setNegativeButtonText(int resId) {
            mNegativeButtonText = mHelper.getContext().getString(resId);
            return this;
        }

        /**
         * Set the theme to be used for the rationale dialog should it be shown.
         *
         * @param theme a style resource
         * @return builder
         */

        public Builder setTheme(int theme) {
            mTheme = theme;
            return this;
        }

        /**
         * Build the permission request.
         *
         * @return the permission request
         * @see EasyPermissions#requestPermissions(PermissionRequest)
         * @see PermissionRequest
         */

        public PermissionRequest build() {
            if (mRationale == null) {
                mRationale = "This app may not work correctly without the requested permissions.";
            }
            if (mPositiveButtonText == null) {
                mPositiveButtonText = "OK";
            }
            if (mNegativeButtonText == null) {
                mNegativeButtonText = "Cancel";
            }

            return new PermissionRequest(
                    mHelper,
                    mPerms,
                    mRequestCode,
                    mRationale,
                    mPositiveButtonText,
                    mNegativeButtonText,
                    mTheme);
        }
    }
}
