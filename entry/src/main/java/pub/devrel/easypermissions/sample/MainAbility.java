/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package pub.devrel.easypermissions.sample;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.window.service.WindowManager;
import ohos.bundle.AbilityInfo;
import ohos.global.configuration.Configuration;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import ohos.utils.PacMap;
import pub.devrel.easypermissions.sample.slice.MainAbilitySlice;

/**
 * 权限申请Demo
 *
 * @since 2021-03-05
 */
public class MainAbility extends Ability {
    private static final HiLogLabel TAG = new HiLogLabel(HiLog.LOG_APP, 0x00201, "ZyTest");
    MainAbilitySlice abilitySlice;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        HiLog.info(TAG, " MainAbility onStart");
        WindowManager.getInstance().getTopWindow().get().
            setStatusBarColor(this.getColor(ResourceTable.Color_theme_color));
        super.setMainRoute(MainAbilitySlice.class.getName());
    }

    @Override
    public void onRequestPermissionsFromUserResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsFromUserResult(requestCode, permissions, grantResults);
        HiLog.info(TAG, "onRequestPermissionsFromUserResult:" + requestCode);
        abilitySlice.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    protected void onAbilityResult(int requestCode, int resultCode, Intent resultData) {
        super.onAbilityResult(requestCode, resultCode, resultData);
        HiLog.info(TAG, "onAbilityResult:回到这里" + requestCode);
        abilitySlice.toast();
    }

    @Override
    protected void onStop() {
        super.onStop();
        HiLog.info(TAG, " MainAbility onStop");
        abilitySlice = null;
    }

    @Override
    protected void onActive() {
        HiLog.info(TAG, " MainAbility onActive");
        super.onActive();
    }

    @Override
    protected void onBackground() {
        HiLog.info(TAG, " MainAbility onBackground");
        super.onBackground();
    }

    @Override
    protected void onForeground(Intent intent) {
        super.onForeground(intent);
        HiLog.info(TAG, " MainAbility onForeground");
    }

    @Override
    protected void onOrientationChanged(AbilityInfo.DisplayOrientation displayOrientation) {
        super.onOrientationChanged(displayOrientation);
        HiLog.info(TAG, " MainAbility onOrientationChanged");
    }

    @Override
    public void onRestoreAbilityState(PacMap inState) {
        super.onRestoreAbilityState(inState);
        HiLog.info(TAG, " MainAbility onRestoreAbilityState");
    }

    @Override
    public void onSaveAbilityState(PacMap outState) {
        HiLog.info(TAG, " MainAbility onSaveAbilityState");
    }

    @Override
    protected void onInactive() {
        super.onInactive();
        HiLog.info(TAG, " MainAbility onInactive");
    }

    @Override
    public void onConfigurationUpdated(Configuration configuration) {
        super.onConfigurationUpdated(configuration);
        HiLog.info(TAG, " MainAbility onConfigurationUpdated");
    }

    public void setAbilitySlice(MainAbilitySlice abilitySlice) {
        this.abilitySlice = abilitySlice;
    }
}
